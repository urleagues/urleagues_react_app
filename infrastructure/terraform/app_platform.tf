resource "digitalocean_app" "urleagues_react_app" {
  spec {
    name   = "urleagues-react-app"
    region = "sfo3"

    domain {
      name = "www.pandascores.com"
      zone = "pandascores.com"
    }

    static_site {
      name          = "react-app"
      build_command = "npm run build"
      output_dir    = "/build"

      gitlab {
        repo = "urleagues/urleagues_react_app"
        branch         = "main"
        deploy_on_push = true
      }
    }

    ingress {
      rule {
        component {
          name = "react-app"
        }
        match {
          path {
            prefix = "/"
          }
        }
      }
    }
  }
}



data "digitalocean_project" "urleagues_project" {
  name = "Urleagues"
}

resource "digitalocean_project_resources" "urleagues" {
  project = data.digitalocean_project.urleagues_project.id
  resources = [
    digitalocean_app.urleagues_react_app.urn
  ]
}
