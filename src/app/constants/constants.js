export const ModalTypes = {
  ADD: Symbol("add"),
  DELETE: Symbol("delete"),
  UPDATE: Symbol("update"),
};
