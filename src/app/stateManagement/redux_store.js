import { configureStore } from '@reduxjs/toolkit';
import GamesReducer from './GamesSlice';

export default configureStore({
  reducer: {
    gamesTable: GamesReducer,
  },
})
